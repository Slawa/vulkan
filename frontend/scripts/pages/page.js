
import ScrollToEl from "./../common/scrollToEl.js";
import Map from "./../common/map.js";
import Plugins from "./../common/plugins.js";

$(function(){
    Plugins.phone('a[href^="tel:"]');
    Plugins.slider('.js-slider');
    ScrollToEl.plugin('body');
    Map.plugin('.js-map', [
        {
            id: 'map-1',
            center: [53.208434571208116,34.45793699999995],
            hintContent: "Центральный офис",
            balllonContent: "Брянск, проспект Московский, 146 <br/> тел +7 (4832) 74-76-74, факс +7 (4832) 74-72-30 <br/>"
        },
        {
            id: 'map-2',
            center: [55.74236448760136,37.560863396820054],
            hintContent: "Московский офис",
            balllonContent: "Москва, ул. Киевская д. 3-7 <br/> (495) 774-32-22  <br/>"
        },
        {
            id: 'map-3',
            center: [53.216497071199925,34.3904285],
            hintContent: "Производство",
            balllonContent: "Брянск, проспект Московский, 10 А <br/> тел +7 (4832) 67-68-72 <br/>"
        }
    ]);


    let $header = $('[data-header-root]');
    $(window).scroll( () =>{
        if ($(window).scrollTop() > 1){
            !$header.hasClass('b-header_state_bg') && $header.addClass('b-header_state_bg');
        } else {
            $header.removeClass('b-header_state_bg');
        }
    });


    $('body')
        .on('click', '[data-toggle-honor]', function (e) {
            e.preventDefault();

            $('[data-honor-root]').addClass('b-honors-con_show');
        })
        .on('click', '[data-toggle-price]', function(e){
            e.preventDefault();
            var $priceRoot = $('[data-price-root]');
            var isShowDetail = !$priceRoot.hasClass('b-price_state_show');

            $priceRoot.toggleClass('b-price_state_show', isShowDetail)
        })
        .on('click', '[data-toggle-menu]', function(e){
            e.preventDefault();
            const $menu = $('[data-header-fixed]');
            const isShowMenu = !$menu.hasClass('b-head_state_menu');

            $menu.toggleClass('b-head_state_menu', isShowMenu);
        });
});