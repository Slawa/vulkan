
let plugins = {};

plugins.phone = (selector) => {
    let deviceAgent = navigator.userAgent.toLowerCase();
    let agentID = deviceAgent.match(/(iphone|ipod|ipad)/);

    if (!agentID) {
        $(selector).each( (index, item) =>{
            let $item  = $(item);
            $item.attr('href', $item.attr('href').replace(/^tel:/, 'callto:'))
        });
    }
};


plugins.slider = (selector) => {
    $(selector).slick({
        dots: true,
        infinite: true,
        autoplay: false,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        arrows: false
    });
};

export default plugins;