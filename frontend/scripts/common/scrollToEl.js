'use strict';

export default class Widget {
    constructor(element) {
        this.$root = $(element);

        this._parseUrl();
        this._assignEvents();
    }

    _parseUrl(){
        const hash = window.location.hash.substr(1);

        if (hash){
            setTimeout( () =>{
                this._scrollTo(hash);
            }, 700);
        }
    }

    _assignEvents() {
        this.$root
            .on('click', '[data-target]', this._onChangeMap.bind(this));
    }

    _onChangeMap(e){
        e.preventDefault();
        const $link = $(e.currentTarget);
        const url = $link.attr('href').split('#');
        const pathname = url[0];
        const target = url[1];

        if(pathname == location.pathname){
            this._scrollTo(target);
        } else {
            window.location.assign($link.attr('href'))
        }
    }

    _scrollTo(hash){
        const $target = $(`[data-id="${hash}"]`);

        if (!$target.length) return;
        $('html, body').animate({
            scrollTop: $target.offset().top
        }, 1000);
    }

    // static
    static plugin(selector) {
        const $elems = $(selector);
        if (!$elems.length) return;

        return $elems.each(function (index, el) {
            let $element = $(el);
            let data     = $element.data('widget.scrollTo');

            if (!data) {
                data = new Widget(el);
                $element.data('widget.scrollTo', data);
            }
        })
    }
}

