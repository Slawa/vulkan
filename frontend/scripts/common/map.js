'use strict';


export default class Widget {

    constructor(element, data) {
        this.$root = $(element);
        this.locals = this._getDom();
        this.mapData = data;
        this.maps = [];

        if (!ymaps){
            console.log('There is no ymaps on the page');
            return;
        }

        ymaps.ready(this._initMap.bind(this));
        this._assignEvents();
    }

    _initMap(){
        for(let mapItem of this.mapData){
            let map = new ymaps.Map(mapItem.id, {
                center: mapItem.center,
                zoom: 16
            });

            let mapPlace = new ymaps.Placemark(mapItem.center, {
                hintContent: mapItem.hintContent,
                balloonContent: mapItem.balloonContent
            });

            map.geoObjects.add(mapPlace);
            this.maps.push(map);
        }
    }

    _getDom(){
        const $root = this.$root;

        return {
            $links: $root.find('[data-map-target]'),
            $maps: $root.find('[data-map-item]'),
            $map: $root.find('[data-map-control]')
        }
    }

    _assignEvents() {
        this.$root
            .on('click', '[data-map-target]', this._onChangeMap.bind(this));
    }

    _onChangeMap(e){
        e.preventDefault();
        const $link = $(e.currentTarget);
        const target = $link.data('map-target');
        const $target = $(target);

        if (!$target.length || $link.hasClass('selected')) return;

       this.locals.$links.removeClass('selected');
        $link.addClass('selected');


        this.locals.$maps.removeClass('state_show');
        $target.addClass('state_show');
    }



    // static
    static plugin(selector, options) {
        const $elems = $(selector);
        if (!$elems.length) return;

        return $elems.each(function (index, el) {
            let $element = $(el);
            let data     = $element.data('widget.map');

            if (!data) {
                data = new Widget(el, options);
                $element.data('widget.map', data);
            }
        })
    }
}

