
var browserSync = require('browser-sync').create();

module.exports = function(options){
    browserSync.init({
        server: {
            baseDir: options.base
        }
    });

    if (!options.pixelperfect){
        browserSync.watch(options.watchSrc).on('change', browserSync.reload);
    }
};