"use strict";

var webpack = require('webpack'),
    config  = require('./gulp/config.js');

module.exports = {
    watch: config.isDev,
    output: {
        filename: '[name].js'
    },
    devtool: config.isDev? false: 'eval-source-map',
    cache: true,
    plugins: [
        new webpack.NoErrorsPlugin()
    ],
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel?presets[]=es2015'
            }
        ]
    },
    resolve: {
        root: __dirname + '/frontend/scripts'
    }
};